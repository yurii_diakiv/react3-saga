import React from 'react';
import ReactDOM from 'react-dom';
// import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Chat from './containers/Chat';
import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import EditPage from './containers/EditPage';
import UserList from './containers/UserList';

ReactDOM.render(
    <React.StrictMode >
        <Provider store={store}>
            <Router>
                <Switch>
                    <Route exact path="/" component={Chat} />
                    <Route exact path="/edit-message" component={EditPage} />
                    <Route exact path="/user-list" component={UserList} />
                </Switch>
            </Router>
        </Provider >
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();