import { 
    createStore, 
    combineReducers, 
    applyMiddleware,
    compose 
} from 'redux';
import chatReducer from './containers/Chat/reducer';
import usersReducer from './containers/UserList/reducer'
import createSagaMiddleware from 'redux-saga';
import chatSagas from './containers/Chat/sagas';
import usersSagas from './containers/UserList/sagas'
import { all } from 'redux-saga/effects';

const initialState = {};

const reducers = {
    chat: chatReducer,
    users: usersReducer
};

const rootReducer = combineReducers({
    ...reducers
});

const sagaMiddleware = createSagaMiddleware();

const composedEnhancers = compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
    // applyMiddleware(sagaMiddleware),
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

function* rootSaga() {
    yield all([
        chatSagas(),
        usersSagas()
    ])
};

sagaMiddleware.run(rootSaga);

export default store;
