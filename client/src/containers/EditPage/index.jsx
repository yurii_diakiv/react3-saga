import React, { useState } from 'react';
import { useHistory } from 'react-router-dom'
import { connect } from 'react-redux';
import {
    editMessage,
    addMessageToEdit
} from '../Chat/actions';

const EditPage = ({
    messageToEdit,
    editMessage
}) => {
    const [body, setBody] = useState(messageToEdit.text);
    const history = useHistory();

    const handleCancelClick = () => {
        history.push('/');
    }
    const handleSaveClick = () => {
        if (body !== '') {
            let newMessage = {};
            Object.assign(newMessage, messageToEdit);
            newMessage.text = body;
            newMessage.editedAt = new Date().toISOString();

            editMessage(newMessage);
            addMessageToEdit();
            history.push('/');
        }
    }
    return (
        <div className='edit-container'>
            <div className='edit-header'>
                Edit Message
        </div>
            <div className='edit-input'>
                <textarea style={{ width: '99%', height: '99%' }} value={body} onChange={ev => setBody(ev.target.value)}></textarea>
            </div>
            <div >
                <button onClick={handleCancelClick}>Cancel</button>
                <button onClick={handleSaveClick}>Save</button>
            </div>
        </div>
    );
};

const mapStateToProps = rootState => ({
    messageToEdit: rootState.chat.messageToEdit
});

const mapDispatchToProps = {
    editMessage,
    addMessageToEdit
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditPage);
