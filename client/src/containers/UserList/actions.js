import { LOAD_USERS } from './actionTypes';

export const loadUsers = () => ({
    type: LOAD_USERS
});
