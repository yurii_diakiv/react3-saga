import React from 'react';
import { connect } from 'react-redux';
import UserItem from '../../components/UserItem';
import {
    loadUsers
} from './actions';

class UserList extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.loadUsers();
    }

    render() {
        return (
            <div>
                {
                    this.props.users.map(user => {
                        return (
                            <UserItem
                                key={user.id}
                                id={user.id}
                                name={user.firstName}
                                surname={user.lastName}
                                email={user.email}
                            />
                        );
                    })
                }
            </div>
        );
    }
}

const mapStateToProps = rootState => ({
    users: rootState.users.users
});

const mapDispatchToProps = {
    loadUsers
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserList);
