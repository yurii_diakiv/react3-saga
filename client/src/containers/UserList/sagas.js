import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
    LOAD_USERS
} from "./actionTypes";

const url = 'http://localhost:3050';

export function* fetchUsers() {
    try {
        const response = yield call(axios.get, `${url}/api/users`);
        yield put({
            type: 'USERS_ACTION:SET_USERS', users: response.data
        })
    } catch (error) {
        console.log('fetchUsers error:', error.message)
    }
}

function* watchFetchUsers() {
    yield takeEvery(LOAD_USERS, fetchUsers)
}

export default function* usersSagas() {
    yield all([
        watchFetchUsers()
    ])
};
