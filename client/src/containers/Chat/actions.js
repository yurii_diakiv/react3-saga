import {
    SET_MESSAGE,
    DELETE_MESSAGE,
    LIKE_MESSAGE,
    EDIT_MESSAGE,
    SET_MESSAGE_TO_EDIT,
    LOAD_MESSAGES
} from './actionTypes';

export const sendMessage = message => ({
    type: SET_MESSAGE,
    message
});

export const loadMessages = () => ({
    type: LOAD_MESSAGES
})

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    id
});

export const likeMessage = message => ({
    type: LIKE_MESSAGE,
    message
});

export const editMessage = message => ({
    type: EDIT_MESSAGE,
    message
});

export const addMessageToEdit = message => ({
    type: SET_MESSAGE_TO_EDIT,
    message
});
