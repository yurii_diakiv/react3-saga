export const SET_ALL_MESSAGES = 'CHAT_ACTION:SET_ALL_MESSAGES';
export const SET_MESSAGE = 'CHAT_ACTION:SET_MESSAGE';
export const DELETE_MESSAGE = 'CHAT_ACTION:DELETE_MESSAGE';
export const LIKE_MESSAGE = 'CHAT_ACTION:LIKE_MESSAGE';
export const EDIT_MESSAGE = 'CHAT_ACTION:EDIT_MESSAGE';
export const SET_MESSAGE_TO_EDIT = 'CHAT_ACTION:SET_MESSAGE_TO_EDIT';
export const LOAD_MESSAGES = 'CHAT_ACTION:LOAD_MESSAGES';
