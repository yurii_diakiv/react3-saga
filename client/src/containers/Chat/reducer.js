import {
    SET_ALL_MESSAGES,
    SET_MESSAGE_TO_EDIT
} from './actionTypes';

export default (state = { messages: [], messageToEdit: undefined }, action) => {
    switch (action.type) {
        case SET_ALL_MESSAGES:
            return {
                ...state,
                messages: action.messages
            };
        case SET_MESSAGE_TO_EDIT:
            return {
                ...state,
                messageToEdit: action.message
            };
        default:
            return state;
    }
};
