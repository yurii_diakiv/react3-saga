import React from 'react';
import { connect } from 'react-redux';

import Header from '../../components/Header';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import Spinner from '../../components/Spinner';

import {
  loadMessages,
  sendMessage,
  deleteMessage,
  likeMessage,
  addMessageToEdit
} from './actions';

class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.sendMessage = this.sendMessage.bind(this);
  }

  sendMessage(message) {
    let newMessage = {
      id: '',
      userId: "1",
      avatar: '',
      user: "Me",
      text: message,
      createdAt: new Date().toISOString(),
      editedAt: ""
    }

    this.props.sendMessage(newMessage);
  }

  componentDidMount() {
    this.props.loadMessages();
  }

  render() {
    return (
      <>
        {
          this.props.messages.length !== 0 ?
            (
              <div className="chat">
                <Header messages={this.props.messages} />
                <MessageList
                  messages={this.props.messages}
                  deleteMessage={this.props.deleteMessage}
                  likeMessage={this.props.likeMessage}
                  addMessageToEdit={this.props.addMessageToEdit}
                />
                <MessageInput sendMessage={this.sendMessage} />
              </div>
            ) :
            <Spinner />
        }
      </>
    )
  }
}

const mapStateToProps = rootState => ({
  messages: rootState.chat.messages
});

const mapDispatchToProps = {
  loadMessages,
  sendMessage,
  deleteMessage,
  likeMessage,
  addMessageToEdit
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
