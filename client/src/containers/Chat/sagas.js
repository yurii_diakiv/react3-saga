import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
    LOAD_MESSAGES,
    SET_MESSAGE,
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    LIKE_MESSAGE
} from "./actionTypes";

const url = 'http://localhost:3050';
const getNewId = () => (new Date()).getTime().toString();

export function* fetchMessages() {
    try {
        const response = yield call(axios.get, `${url}/api/messages`);
        const messages = response.data.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
        yield put({
            type: 'CHAT_ACTION:SET_ALL_MESSAGES', messages: messages
        })
    } catch (error) {
        console.log('fetchMessages error:', error.message)
    }
}

function* watchFetchMessages() {
    yield takeEvery(LOAD_MESSAGES, fetchMessages)
}

export function* sendMessage(action) {
    const newMessage = action.message;
    newMessage.id = getNewId();
    try {
		yield call(axios.post, `${url}/api/messages`, action.message);
		yield put({ type: LOAD_MESSAGES });
	} catch (error) {
		console.log('sendMessage error:', error.message);
	}
}

function* watchSendMessage() {
	yield takeEvery(SET_MESSAGE, sendMessage)
}

export function* deleteMessage(action) {
    try {
        yield call(axios.delete, `${url}/api/messages/${action.id}`);
        yield put({ type: LOAD_MESSAGES })
    } catch (error) {
        console.log('deleteMessage Error:', error.message);
    }
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export function* updateMessage(action) {
    const { id, ...newData } = action.message;

    try {
		yield call(axios.put, `${url}/api/messages/${id}`, newData);
		yield put({ type: LOAD_MESSAGES });
	} catch (error) {
		console.log('updateMessage error:', error.message);
	}
}

function* watchUpdateMessage() {
	yield takeEvery(EDIT_MESSAGE, updateMessage)
}

function* watchLikeMessage() {
	yield takeEvery(LIKE_MESSAGE, updateMessage)
}

export default function* chatSagas() {
    yield all([
        watchFetchMessages(),
        watchSendMessage(),
        watchDeleteMessage(),
        watchUpdateMessage(),
        watchLikeMessage()
    ])
};
