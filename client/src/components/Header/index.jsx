import React from 'react';

const Header = ({
  messages
}) => {
    return (
      <div className="header">
        <p>My chat</p>
        <p>{new Set(messages.map(m => m.userId)).size} participants</p>
        <p>{messages.length} messages</p>
        <p style={{marginLeft: 'auto'}}>Last message at {new Date(messages[messages.length - 1]?.createdAt).toLocaleString()}</p>
      </div>
    );
}

export default Header;