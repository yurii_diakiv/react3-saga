import React from "react";

const UserItem = ({
    name,
    surname,
    email
}) => {
    return (
        <div>
            <span>{name} {surname}</span>
            <span style={{marginLeft: '10px'}}>{email}</span>
        </div>
    );
};

export default UserItem;