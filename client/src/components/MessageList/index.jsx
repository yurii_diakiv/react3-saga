import React from 'react';
import Message from '../Message';

const MessageList = ({
  messages,
  deleteMessage,
  likeMessage,
  addMessageToEdit
}) => {
  return (
    <div className="message-list">
      { messages.map((m, index, array) =>
        <React.Fragment key={m.id}>
          <Message
            m={m}
            deleteMessage={deleteMessage}
            likeMessage={likeMessage}
            addMessageToEdit={addMessageToEdit}
            isLast={(index === array.length - 1) && true}
          />
          {new Date(new Date(m.createdAt).getFullYear(), new Date(m.createdAt).getMonth(), new Date(m.createdAt).getDate()) <
            new Date(new Date(array[index + 1]?.createdAt).getFullYear(), new Date(array[index + 1]?.createdAt).getMonth(), new Date(array[index + 1]?.createdAt).getDate())
            &&
            <div className="divider">
              {new Date(array[index + 1].createdAt).toDateString()}
            </div>}
        </React.Fragment>
      )}
    </div>
  );
}

export default MessageList;