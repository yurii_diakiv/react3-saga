import React, { useState } from 'react';
import { sendMessage } from '../../containers/Chat/sagas';

const MessageInput = (
  {
    sendMessage
  }
) => {

  const [value, setValue] = useState('');

  const handleChange = (event) => {
    setValue(event.target.value);
  }

  const handleClick = () => {
    if (value !== '') {
      sendMessage(value);
      setValue('');
    }
  }

  return (
    <div className="input-wrapper">
      <input type="text" className="message-input" autoFocus placeholder="Message" value={value} onChange={handleChange} />
      <button onClick={handleClick}>Send</button>
    </div>
  );
}

export default MessageInput;
