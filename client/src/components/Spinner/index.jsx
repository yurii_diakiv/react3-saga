import React from 'react';

const Spinner = () => {
    return (
      <div className="flex-centered height-fullscreen">
        <div className="loader"></div>
      </div>
    );
}

export default Spinner;