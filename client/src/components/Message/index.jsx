import React from 'react';
import { Heart, HeartFill, Trash, Gear } from 'react-bootstrap-icons';
import { useHistory } from 'react-router-dom'

const Message = ({
  m,
  deleteMessage,
  likeMessage,
  addMessageToEdit,
  isLast
}) => {
  const history = useHistory();

  const handleDeleteClick = () => {
    deleteMessage(m.id);
  }

  const handleLikeClick = () => {
    let likedMessage = {};
    Object.assign(likedMessage, m);
    likedMessage.isLike = !m.isLike;
    likeMessage(likedMessage);
  }

  const handleEditClick = () => {
    addMessageToEdit(m);
    history.push('/edit-message');
  }

  return (
    <div className="message">
      <div className="message-main">
        {m.avatar && <img src={m.avatar} />}
        <p>{m.text}</p>
      </div>
      <div className="message-secondary">
        <p style={{ fontSize: '13.5px' }}>{new Date(m.createdAt).toLocaleString()}</p>
        {m.userId === '1' &&
          <Trash className="icon-button" onClick={handleDeleteClick} />}
        {m.userId === '1' && isLast &&
          <Gear className="icon-button edit-button" onClick={handleEditClick} />}
        {m.userId !== '1' &&
          (m.isLike === true ?
            <HeartFill className="icon-button" onClick={handleLikeClick} /> :
            <Heart className="icon-button" onClick={handleLikeClick} />)}
      </div>
    </div>
  );
}

export default Message;
