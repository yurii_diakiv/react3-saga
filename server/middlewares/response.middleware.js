const responseMiddleware = (req, res, next) => {
   if(res.locals.validationErr){
       res.status(400).json({ "error": true, "message": res.locals.validationErr});
   }
   else if(res.locals.err) {
       res.status(404).json({"error": true, "message": res.locals.err});
   }
   else {
    res.status(200).json(res.locals.data);
   }
    next();
}

exports.responseMiddleware = responseMiddleware;