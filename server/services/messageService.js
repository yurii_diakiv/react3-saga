const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
    getAll() {
        return MessageRepository.getAll();
    }

    create(message) {
        return MessageRepository.create(message);
    }

    delete(id) {
        const result = MessageRepository.delete(id);
        if (!result.length) {
            throw new Error('Message not found');
        }
        return result;
    }

    update(id, data) {
        const result = MessageRepository.update(id, data);
        if (!result.id) {
            throw new Error('Message not found(wrong id)');
        }
        return result;
    }
}

module.exports = new MessageService();
