const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router
    .get('', (req, res, next) => {
        try {
            const data = MessageService.getAll();
            res.locals.data = data;
        } catch (err) {
            res.locals.err = err.message;
        } finally {
            next();
        }
    }, responseMiddleware)
    .post('', (req, res, next) => {
        try {
            const data = MessageService.create(req.body);
            res.locals.data = data;
        } catch (err) {
            res.locals.err = err.message;
        } finally {
            next();
        }
}, responseMiddleware)
.delete('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        const data = MessageService.delete(id);
        res.locals.data = data;
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware)
.put('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        const data = MessageService.update(id, req.body);
        res.locals.data = data;
    } catch(err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
