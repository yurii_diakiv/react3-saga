const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
//s
router.get('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        const data = UserService.search({ id });
        res.locals.data = data;
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('', (req, res, next) => {
    try {
        const data = UserService.getAll();
        res.locals.data = data;
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('', createUserValid, (req, res, next) => {
    try {
        if (!res.locals.validationErr) {
            const data = UserService.create(req.body);
            res.locals.data = data;
        }
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        if (!res.locals.validationErr) {
            let id = req.params.id;
            const data = UserService.update(id, req.body);
            res.locals.data = data;
        }
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        const data = UserService.delete(id);
        res.locals.data = data;
    } catch (err) {
        res.locals.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware);
//e

module.exports = router;